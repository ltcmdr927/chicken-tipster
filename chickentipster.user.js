// ==UserScript==
// @name Chicken Tipster
// @namespace
// @description ヤフー！ファイナンスの株価予想で8時58分〜9時00分に投稿されたものを薄くする
// @includes http://info.finance.yahoo.co.jp/kabuyoso/article/*
// @includes 
// ==/UserScript==

var conditionTimes = ["8時58分", "8時59分", "9時00分"];
var conditionSections = ["NewForecast"];
var conditionClasses = ["st04 yjS"];

[].forEach.call(conditionTimes, function(conditionTime) {
  [].forEach.call(conditionSections, function(conditionSection){
    var section = document.getElementsByClassName(conditionSection)[0];
    if (section !== undefined) {
      [].forEach.call(conditionClasses, function(conditionClass) {
        var list = section.getElementsByClassName(conditionClass);
        [].forEach.call(list, function(item){
          if(item.innerHTML.indexOf(conditionTime) > -1) {
            var target = item.parentNode.parentNode.parentNode.parentNode;
            target.style.opacity = 0.1;
          }
        });
      });
    }
  });
});
