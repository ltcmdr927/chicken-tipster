# chickentipster.user.js

## 概要

### これについて

ヤフー！ファイナンスの株価予想で8時58分〜9時00分に投稿されたものを薄くします。

### なぜ作ったのか

http://anago.2ch.sc/test/read.cgi/stock/1407206327/432

## 使い方

### インストール方法

設定→拡張機能で chickentipster.user.js を Google Chrome にドラッグ＆ドロップすれば入ると思います。

### カスタマイズ

conditionTimes には任意の時間を入れられます。12時29分を指定するのも良いと思います。
